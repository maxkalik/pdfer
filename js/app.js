const formPDFer = document.getElementById('pdfer');
const inputFields = document.getElementById('inputs');
const inputURL = document.getElementById('url');
const errorMsg = document.getElementById('error');
const resetBtn = document.getElementById('reset');
const submitBtn = document.getElementById('submit');

console.log(inputFields);


const validateURL = (url) => {

    formPDFer.classList.remove('invalid');
    inputFields.classList.remove('invalid');
    formPDFer.classList.remove('valid');
    inputFields.classList.remove('valid');
    errorMsg.style.display = 'none';
    submitBtn.disabled = true;

    url = inputURL.value;

    if (url !== '') {

        const res = url.match(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/g);

        resetBtn.style.display = 'block';

        if (res == null) {

            showError();
            submitBtn.disabled = true;

        } else {
            
            const resSecond = url.match(/^http(s)?:\/\/*/);
            
            submitBtn.disabled = false;
            formPDFer.classList.add('valid');
            inputFields.classList.add('valid');

            if (resSecond == null) {
                return `http://${url}`
            } else {
                return url         
            }
        }
    } else {
        resetBtn.style.display = 'none';
        submitBtn.disabled = true;
    }
}

formPDFer.addEventListener('submit', (e) => {

    window.open(`https://api.pdfer.ru/?url=${validateURL()}`, '_blank')
    e.preventDefault();
    
});

inputURL.addEventListener('keyup', validateURL);

resetBtn.addEventListener('click', () => {
    inputURL.value === '';
    resetBtn.style.display = 'none';
    formPDFer.classList.remove('invalid');
    inputFields.classList.remove('invalid');
    formPDFer.classList.remove('valid');
    inputFields.classList.remove('valid');
    errorMsg.style.display = 'none';
    submitBtn.disabled = true;
});

function showError() {
    formPDFer.classList.add('invalid');
    inputFields.classList.add('invalid');
    errorMsg.style.display = 'block';
    submitBtn.disabled = true;
}